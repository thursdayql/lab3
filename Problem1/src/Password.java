
//********************************************************************
//File:	        Password.java       
//Author:       Liam Quinn
//Date:	        September 21st 2017
//Course:       CPS100
//
//Problem Statement:
// Write an application that prompts for and reads the user's first and last 
// name (separately). Then print a string composed of the first letter of the 
// user's first name, followed by the first five characters of the user's last 
// name, followed by a random number in the range 10 to 99. Assume that the last
// name is at least five letters long. Similar algorithms are sometimes used 
// to generate usernames for new computer accounts.
//
// Inputs:	First Name, Last Name (Both Strings)
// Outputs:	Created Password
// 
//********************************************************************

import java.util.*;


public class Password
{

  public static void main(String[] args)
  {
    // 1. Declare variables
    int number = -1;
    String firstName = "";
    String lastName = "";
    char firstCharacter = ' ';
    String fiveCharString = "";
    Scanner scan = new Scanner(System.in);
    Random generator = new Random();
    String password = "";

    // 2. Get first name and display it
    System.out.println("Enter your first name: ");
    firstName = scan.next();
    //System.out.println("Your first name is " + firstName);

    // 3. Get last name and display it
    System.out.println("Enter your last name: ");
    lastName = scan.next();
    //System.out.println("Your last name is " + lastName);

    // 4. Get the first character of the first name and display it
    firstCharacter = firstName.charAt(0);
    //System.out.println("First Character = " + firstCharacter);

    // 5. Get the first five characters of the last name and display it
    fiveCharString = lastName.substring(0, 5);
    //System.out.println("First five characters of last name = " + fiveCharString);

    // 6. Generate random number between 10-99
    number = generator.nextInt(90) + 10;
    //System.out.println("Random integer between 10 and 99 = " + number);

    // 7. Create the password and display it
    password = "" + firstCharacter + fiveCharString + number;
    System.out.println("Your password is " + password);

  }

}
