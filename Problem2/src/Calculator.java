//********************************************************************
//File:         Calculator.java       
//Author:       Liam Quinn
//Date:         September 21st 2017
//Course:       CPS100
//
//Problem Statement:
// Write an application that prints the sum of cubes. Prompt for and read 
// two integer values and print the sum of each value raised to the third power.
//

//
//Inputs: Get two float numbers  
//Outputs:  Display sum of cubes of both numbers
// 
//********************************************************************

import java.util.*;

public class Calculator
{

  public static void main(String[] args)
  {
    int number1 = 0;
    int number2 = 0;
    Scanner keyBoard = new Scanner(System.in);
    
    // read both number
    System.out.println("Enter first number: ");
    number1 = (int)keyBoard.nextDouble();
    System.out.println("Enter second number: ");
    number2 = (int)keyBoard.nextDouble();
    
    number1 = (int)Math.pow(number1, 3);
    number2 = (int)Math.pow(number2,  3);
    System.out.println("First number cubed = " + number1);
    System.out.println("Second number cubed = " + number2);
    
    System.out.println("Sum of both cubed numbers = " + (number1 + number2));;

  }

}
